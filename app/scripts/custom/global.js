var initGlobal = function() {

	/* 
		Speaker Carousel 
		
		Uses SlickJS Plugin - http://kenwheeler.github.io/slick/
	*/

	$('[data-js="carousel"]').slick({
		  dots: true,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  responsive: [
			{
			  arrows: false,
			  breakpoint: 960,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  arrows: false,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		  ]
	});


	/* 
		Accordion
	*/

	$('[data-js="accordion"]').each(function(){

		var $this = $(this),
			$item = $('[data-js="accordionItem"]'),
			$trigger = $item.find('[data-js="accordionTrigger"]'),
			$content = $item.find('[data-js="accordionContent"]'),
			events = $('html').hasClass('desktop') ? 'mouseenter click' : 'click';


		/* If Desktop/no-touch, toggle active class on hover
		   Else, trigger on click/touch */
		
		$trigger.on(events, function(){
			var $parent = $(this).parents('[data-js="accordionItem"]');

			$item.not($parent).removeClass('is-active');
			$parent.toggleClass('is-active');
		});
	});


	/* 
		Attendee form
	*/

	$('[data-js="attendeeForm"]').each(function(){

		var $attendeeForm = $(this),
			$initForm = $attendeeForm.find('.form-fields--initial'),
			$addAttendeeButton = $attendeeForm.find('[data-js="addAttendee"]'),
			$formItems = $attendeeForm.find('[data-js="formItems"]'),
			attendeeName = $('[data-js="attendeeName"]'),
			attendeeTitle = $('[data-js="attendeeTitle"]'),
			$editButton = $('[data-js="editAttendee"]'),
			$deleteButton = $('[data-js="deleteAttendee"]'),
			numAttendees = 0,
			$currentAttendee = $('.form-item'),
			$saveButton = $('[data-js="saveAttendee"]');

			
		/* 
			Initial state of the form 
			hides save button, adds Add attendee button
		*/
		var init = function(){
			if ( numAttendees < 1 ) {

				if ($formItems.children().length = 1 ) {
					$addAttendeeButton.removeClass('is-hidden');
					$saveButton.addClass('is-hidden');
				}
			} 
		}();


		/* 
			Delete button removes item, reduces attendee count
		*/
		$('body').on('click', '.form-itemButtonDelete', function(event){
			event.preventDefault();

			$(this).parents('.form-item').remove();
			numAttendees--;
		});


		/* 
			Edit button displays form
		*/
		$('body').on('click', '.form-itemButtonEdit', function(event){
			event.preventDefault();

			var thisItem = $(this).parents('.form-item');

			thisItem.addClass('is-active');
			thisItem.find( $saveButton ).removeClass('is-hidden');
		});


		/* 
			Save button triggers save function
		*/

		$('body').on('click', '.form-buttonSave', function(event){

			event.preventDefault();

			var thisItem = $(this).parents('.form-item');

			saveAttendeeDetails(thisItem);
		});


		/* 
			Add attendee button adds new form, hides Add button
		*/

		$addAttendeeButton.on('click', function(event){

			event.preventDefault();

			// If this is the initial form, update and show header
			addNewForm();

			// Hide Add attendee button
			$addAttendeeButton.addClass('is-hidden'); 

			if ( numAttendees < 1 ) {

				if ($formItems.children().length = 1 ) {
					$saveButton.trigger('click');
				}
			} 

			// Update number of attendees
			numAttendees++;
		});

	
		/* 
			Save attendee details updates summary text, hides form
		*/

		function saveAttendeeDetails( attendee ){

			var firstName = attendee.find('.form-input--firstName').val(),
				lastName = attendee.find('.form-input--lastName').val(),
				$summary = attendee.find('[data-js="attendeeSummary"]');

			$summary.html(firstName + " " + lastName);

			// Hide form & show summary text
			attendee.removeClass('is-new').removeClass('is-active');

			// Show Add attendee button
			$addAttendeeButton.removeClass('is-hidden');

			/* Remove any extra checkboxes */
			$('.form-fields').each(function(){
				$(this).find('.controller.checkbox .controller.checkbox .form-checkboxLabel:gt(0)').remove();
			});	
		}


		/* 
			Add new form adds a new form template to $formItems,
			reinitialises form JS, updates form attributes
		*/

		function addNewForm(){

			var $attendeeItem = $('<div class="form-item" />').html( $('#attendeeFormTemplate').html() ),
				$saveButton = $('<a class="form-buttonSave" href="" data-js="saveAttendee"><span>Save</span>
			</a> ');

			/* Add Save button and prepend form to indicent container */
			$saveButton.appendTo( $attendeeItem );
            $attendeeItem.addClass('is-new').addClass('is-active').appendTo( $formItems );
        	
            /* Give checkboxes unique ID */
            $attendeeItem.find('.form-checkbox').attr('id', 'checkbox' + numAttendees );
            $attendeeItem.find('.form-checkboxLabel').attr('for', 'checkbox' + numAttendees);
        	
        	/* Reinitalise forms */
        	initForm();
		}
	});
}
