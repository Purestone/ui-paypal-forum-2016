## PayPal UK Business Forum

Based on Matter **SASS** front-end framework.

## Browser/OS Support / Tested on:

###Windows 10
IE11
Chrome (48)
Firefox (44)

###OSX Yosemite 
Chrome (48)
Firefox (44)
Safari (9)

###iOS 9.x (latest)
iPhone 4S / iPad 3 - Mobile Safari

###Android 4.4 +
Google Nexus 7 Tablet, Samsung Galaxy SIII phone - Chrome, Android Browser v47



[](#)

## Get Started

#### 1\. Requirements

`Note: Use **sudo** on *nix systems to install node packages.`

1.  [Node](http://nodejs.org/)
2.  [Grunt](https://www.npmjs.com/package/grunt) `npm install -g grunt`
3.  [Grunt Command Line Interface (CLI)](https://www.npmjs.com/package/grunt-cli) `npm install -g grunt-cli`

* * *

#### 2\. Grunt

**First run?** Navigate to Matter's directory and run `npm install` then run `grunt`.

**Returning?** Simply run `grunt`.

* * *
